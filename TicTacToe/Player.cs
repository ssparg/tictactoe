﻿namespace TicTacToe
{
    class Player
    {
        public string Name { get; }
        public byte Number { get; }
        public char Sign { get; }
        public bool Turn { get; set; }

        public Player(string name, char sign, byte number, bool turn = false)
        {
            Name = name;
            Sign = sign;
            Number = number;
            Turn = turn;
        }
    }
}
