﻿using System;
using System.Collections;

namespace TicTacToe
{
    class Program
    {
        static Player player1;

        static Player player2;

        static char[,] playField =
        {
            {'1', '2', '3'},
            {'4', '5', '6'},
            {'7', '8', '9'},
        };

        static byte turns = 0;

        static void Main(string[] args)
        {
            byte input = 0;
            string currentPlayer = "";
            char currentSign = ' ';
            bool inputCorrect;

            Initialize();

            do
            {
                if (player1.Turn == true)
                {
                    currentPlayer = player1.Name;
                    currentSign = player1.Sign;

                }
                else if (player2.Turn == true)
                {
                    currentPlayer = player2.Name;
                    currentSign = player2.Sign;
                }

                CreateGrid();
                #region
                ArrayList players = new ArrayList(2)
                {
                    player1,
                    player2
                };

                foreach (Player player in players)
                {

                    if (((playField[0, 0] == player.Sign) && (playField[0, 1] == player.Sign) && (playField[0, 2] == player.Sign))
                        || ((playField[1, 0] == player.Sign) && (playField[1, 1] == player.Sign) && (playField[1, 2] == player.Sign))
                        || ((playField[2, 0] == player.Sign) && (playField[2, 1] == player.Sign) && (playField[2, 2] == player.Sign))
                        || ((playField[0, 0] == player.Sign) && (playField[1, 0] == player.Sign) && (playField[2, 0] == player.Sign))
                        || ((playField[0, 1] == player.Sign) && (playField[1, 1] == player.Sign) && (playField[2, 1] == player.Sign))
                        || ((playField[0, 2] == player.Sign) && (playField[1, 2] == player.Sign) && (playField[2, 2] == player.Sign))
                        || ((playField[0, 0] == player.Sign) && (playField[1, 1] == player.Sign) && (playField[2, 2] == player.Sign))
                        || ((playField[0, 2] == player.Sign) && (playField[1, 1] == player.Sign) && (playField[2, 0] == player.Sign)))
                    {

                        Console.WriteLine("\n{0} has won!", player.Name);
                        Console.WriteLine("Please press any key to reset the game!");
                        Console.ReadKey();

                        Reset();

                        break;
                    }
                    else if (turns == 10)
                    {
                        Console.WriteLine("\nDraw!");
                        Console.WriteLine("Please press any key to reset the game!");
                        Console.ReadKey();

                        Reset();

                        break;
                    }
                }
                #endregion

                #region
                // Test field is already taken
                do
                {
                    Console.Write("\n{0}: Choose your field! ", currentPlayer);
                    try
                    {
                        input = Convert.ToByte(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Please enter a number!");
                    }

                    if ((input == 1) && playField[0, 0] == '1')
                        inputCorrect = true;
                    else if ((input == 2) && playField[0, 1] == '2')
                        inputCorrect = true;
                    else if ((input == 3) && playField[0, 2] == '3')
                        inputCorrect = true;
                    else if ((input == 4) && playField[1, 0] == '4')
                        inputCorrect = true;
                    else if ((input == 5) && playField[1, 1] == '5')
                        inputCorrect = true;
                    else if ((input == 6) && playField[1, 2] == '6')
                        inputCorrect = true;
                    else if ((input == 7) && playField[2, 0] == '7')
                        inputCorrect = true;
                    else if ((input == 8) && playField[2, 1] == '8')
                        inputCorrect = true;
                    else if ((input == 9) && playField[2, 2] == '9')
                        inputCorrect = true;
                    else
                    {
                        Console.WriteLine("\n Incorrect input! Please use another field!");
                        inputCorrect = false;
                    }

                    if (inputCorrect)
                    {
                        EnterXorO(currentSign, input);
                        SwitchTurns();
                    }

                } while (!inputCorrect);

            } while (true);
            #endregion
        }

        private static void Reset()
        {
            char[,] playFieldInitial =
            {
                {'1', '2', '3'},
                {'4', '5', '6'},
                {'7', '8', '9'},
            };
            playField = playFieldInitial;
            CreateGrid();
            turns = 0;
        }

        private static void CreateGrid()
        {
            Console.Clear();
            Console.WriteLine("     |     |     ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", playField[0, 0], playField[0, 1], playField[0, 2]);
            Console.WriteLine("_____|_____|_____");
            Console.WriteLine("     |     |     ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", playField[1, 0], playField[1, 1], playField[1, 2]);
            Console.WriteLine("_____|_____|_____");
            Console.WriteLine("     |     |     ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", playField[2, 0], playField[2, 1], playField[2, 2]);
            Console.WriteLine("_____|_____|_____");
            Console.WriteLine("     |     |     ");
            turns++;
        }

        private static void EnterXorO(char playerSign, int input)
        {
            switch (input)
            {
                case 1: playField[0, 0] = playerSign; break;
                case 2: playField[0, 1] = playerSign; break;
                case 3: playField[0, 2] = playerSign; break;
                case 4: playField[1, 0] = playerSign; break;
                case 5: playField[1, 1] = playerSign; break;
                case 6: playField[1, 2] = playerSign; break;
                case 7: playField[2, 0] = playerSign; break;
                case 8: playField[2, 1] = playerSign; break;
                case 9: playField[2, 2] = playerSign; break;
            }
        }

        private static void Initialize()
        {
            Console.WriteLine("\nPlease enter the name of player 1 or press enter for default");
            string playerOne = Console.ReadLine();
            playerOne = IsNameEmpty(playerOne) ? "Player1" : playerOne;
            player1 = new Player(playerOne, 'X', 1, true);
            Console.WriteLine("\nPlease enter the name of player 2 or press enter for default");
            string playerTwo = Console.ReadLine();
            playerTwo = IsNameEmpty(playerTwo) ? "Player2" : playerTwo;
            player2 = new Player(playerTwo, 'O', 2);
        }

        private static bool IsNameEmpty(string name)
        {
            return string.IsNullOrEmpty(name);
        }

        private static void SwitchTurns()
        {
            if (player1.Turn == true)
            {
                player1.Turn = false;
                player2.Turn = true;
            }
            else if (player2.Turn == true)
            {
                player2.Turn = false;
                player1.Turn = true;
            }
        }
    }
}

